import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, last, map, retry } from 'rxjs/operators';
import {Employee} from 'src/app/Models/Employee';
import { environment } from 'src/environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
  providedIn:'root'
})
export class EmployeeService {
  constructor(private http: HttpClient) { }



  getNhanVien(){
      return this.http.get(environment.backend + '/user/getAllUser');
  }
  createNhanvien(employee :Employee){
    return this.http.post(environment.backend + '/user/createUser',employee);
  }
  getIDUSer(id:number){
    return this.http.post(environment.backend + '/user/getIDUSer',{ID:id });
  }
  updateEmployee(id:number,firstname:string,lastname:string){
    return this.http.post(environment.backend+'/user/updateUser',{ID:id , firstname:firstname,lastname:lastname});
  }




  
}