export interface Employee{
    id:string;
    anhcanhan:string;   
    firstname:string;
    lastname:string;
    middlename:string;
    maNV:string;
    chucvu:string;
    gioitinh:string;
    email:string;
    sdt:string;
    ext:string; 
    ngaysinh:string;
    noisinh:string;
    nguyenquan:string;
    tongiao:string;
    dantoc:string;
    cmnd:string;
    ngaycapcmnd:string;
    noicapcmnd:string;
    sohochieu:string;
    ngayhethanhochieu:string;
    ngaycaphochieu:string,
    noicaphochieu:string;
    masothue:string;
    quoctich:string;
    diachithuongtru:string;
    diachihokhau:string;
    calamviec:string;
    trangthailamviec:string;
    machamcong:string;
   
  }
  

  
  