import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Task1Component } from './task1/task1.component';

import {MatInputModule} from '@angular/material/input';

import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { ReactiveFormsModule } from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { FooterComponent } from './shared/components/footer/footer.component';
import { DetailComponent } from './Component/detail/detail.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import { HeaderComponent } from './shared/components/header/header.component'
import { HttpClientModule } from '@angular/common/http';
import {EmployeeService}from 'src/app/shared/services/employee.service';
import { ListComponent } from './Component/list/list.component';
import {MatDialogModule} from '@angular/material/dialog';
import { CreateUserComponent } from './Component/create-user/create-user.component';
import { FormsModule } from '@angular/forms';





@NgModule({
  declarations: [
    AppComponent,
    Task1Component,
    FooterComponent,
    DetailComponent,
    HeaderComponent,
    ListComponent,
    CreateUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatSelectModule,
    MatTooltipModule,MatButtonToggleModule,ReactiveFormsModule,
    MatCardModule,
    MatSidenavModule,
    MatMenuModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatIconModule,
    HttpClientModule,MatDialogModule,
    FormsModule



  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
