import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl, FormBuilder} from '@angular/forms';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-task1',
  templateUrl: './task1.component.html',
  styleUrls: ['./task1.component.scss']
})
export class Task1Component implements OnInit {

  constructor(private fb:FormBuilder) { }

  ngOnInit(): void {
  }
  pattern1="/^\d{8,12}$/)"

  genders=[{id : 1, name:'Male'},{id : 2, name:'Female'},{id : 3, name:'Other'},{id : 4, name:'I do not wish to say'}];
  info=new FormGroup({
    name:new FormControl('Trần Minh Triết',[Validators.required]),
    contact :new FormControl('0931323148',[Validators.pattern(/^\d{8,12}$/)]),
    email : new FormControl('tranminhtriet1402@gmail.com',[Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
    gender : new FormControl(this.genders[0].name,[Validators.required]),
  note : new FormControl('Hello',[Validators.required])
  });

  onSubmit() {
    console.warn(this.info.value);
  } 
  
  showDiv = {
    text : true,
    edit_text:false,
    button_edit:true,
    button_save:false,
  }

  show_edit_text(){
    this.showDiv.text = !this.showDiv.text;
    this.showDiv.edit_text = true;
    this.showDiv.button_edit=false;
    this.showDiv.button_save=true

  }

  show_text(){
    this.showDiv.text = !this.showDiv.text;
    this.showDiv.edit_text = false;
    this.showDiv.button_edit=true;
    this.showDiv.button_save=false
  }

  

  

}


