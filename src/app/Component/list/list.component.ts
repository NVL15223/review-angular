import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import {EmployeeService}from 'src/app/shared/services/employee.service';
import {MatPaginator} from '@angular/material/paginator';
import {Employee} from 'src/app/Models/Employee';
import {MatDialog} from '@angular/material/dialog';
import { CreateUserComponent } from '../create-user/create-user.component';
import { ActionSequence } from 'selenium-webdriver';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  displayedColumns: string[] = ['position','image', 'tenCBNV', 'chucvu', 'gender','maNV','email','contact','ext','thaotac'];
  headElements = ['ID', 'Hình Ảnh', 'Tên CBNV', 'Chức vụ','gender','Mã NV','Email','Contact','Ext','Thao tác'];
  public dataSource = new MatTableDataSource<Employee>();

  constructor(private employeeService:EmployeeService, public dialog :MatDialog , private _route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getNhanvien();
    

  }

  getNhanvien(){
    this.employeeService.getNhanVien().subscribe(
      (res:any)=>{
        if(res.status == "success"){    
          this.dataSource.data=res.responseData as Employee[];
          console.log(this.dataSource.data) 
        }else{
          alert(res.message)
        }
      }
    );  
  }

 





////////////////
@ViewChild(MatPaginator) paginator!: MatPaginator;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;    
  }
  disableSelect = new FormControl(false);
  selectedValue!: string;
  selectedCar!: string;
  form = new FormGroup({
    cols: new FormControl(""),
    input:new FormControl("")
  });
  cols: Col[] = [
    {value: 'ten', viewValue: 'Tên'},
    {value: 'ma', viewValue: 'Mã'},
    {value: 'email', viewValue: 'Email'},
    {value: 'sdt', viewValue: 'SĐT'}   
  ];
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return data.firstname.toLowerCase().includes(filter) || data.lastname.toLowerCase().includes(filter) || data.middlename.toLowerCase().includes(filter);
  };
  }
  applyFilter1(event: Event) 
  {
    if(this.selectedValue=="ten")
    {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.lastname.toLowerCase().includes(filter) || data.firstname.toLowerCase().includes(filter) ;
      };
    }
    if(this.selectedValue=="ma")
    {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.maNV.toString().toLowerCase().includes(filter) ;
      };
    }
    if(this.selectedValue=="sdt")
    {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.sdt.toString().toLowerCase().includes(filter) ;
      };
    }
    if(this.selectedValue=="email")
    {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
      this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.email.toLowerCase().includes(filter) ;
      };
    }
  }
  search={
    sr1:true,
    sr2:false,
  }
  show1(){
    this.search.sr1=!this.search.sr1,
    this.search.sr2=true
  }
  show2(){
    this.search.sr2=!this.search.sr2,
    this.search.sr1=true
  }

  openDialog(){
    this.dialog.open(CreateUserComponent);
  }
}

interface Col {
  value: string;
  viewValue: string;
}
