import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { Validators } from '@angular/forms';
import {EmployeeService}from 'src/app/shared/services/employee.service';
import { Employee } from '../../Models/Employee';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
 

  constructor(private fb : FormBuilder, private employeeService:EmployeeService, private _Activatedroute:ActivatedRoute) {
   }


 

  //  user:Employee={id:'',
  //  anhcanhan:'',
  //  firstname:'',
  //  middlename:'',
  //  lastname:'',maNV:''
  //  ,chucvu:'',gioitinh:'',
  //  email:'',
  //  sdt:'',
  //  ext:'',
  //  ngaysinh:'',
  //  noisinh:'',
  //  nguyenquan:'',
  //  tongiao:'',
  //  dantoc:'',
  //  cmnd:'',
  //  ngaycapcmnd:'',
  //  sohochieu:'',
  //  ngayhethanhochieu:'',
  //  ngaycaphochieu:'',
  //  noicaphochieu:'',
  //  masothue:'',
  //  quoctich:'',
  //  diachithuongtru:'',
  //  diachihokhau:'',
  //  noicapcmnd:'',
  //  machamcong:'',
  //  trangthailamviec:'',
  //  calamviec:'', 
  //  }

   user:any=[]

 




  IDUSER: number=0;
  private sub: any;

  ngOnInit(): void {
    this.getIDUSer();
  }

  getIDUSer(){
    ///Route paramente  
    this.sub = this._Activatedroute.params.subscribe(params => {
      this.IDUSER = +params['id']; 
   });
    this.employeeService.getIDUSer(this.IDUSER).subscribe(
      (res:any)=>{
        if(res.status == "success"){   
          this.user = res.responseData;  
          console.log(this.user)
          // this.user.data=res.responseData as User[];
        }else{
          console.log("fail") 
        }
      }
    )
  }
  updateEmployee(){
    this.sub=this._Activatedroute.params.subscribe(params=>{
      this.IDUSER=+params['id']
    })

    var firstname=this.thongTinCaNhan.controls['ho'].value;
    let lastname=this.thongTinCaNhan.controls['ten'].value;


    this.employeeService.updateEmployee(this.IDUSER,firstname,lastname).subscribe(
      (res:any)=>{
        if(res.status =="success"){
          // this.user=res.responseData;
          console.log(this.user);
          window.location.reload();   
        }else{
          console.log('fail')
        }
      }
    )
  

  }

 

  

  /////////
  currentDate : Date =new Date();
  thongTinCaNhan = new FormGroup({
    ho: new FormControl('',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    tenlot: new FormControl('',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    ten: new FormControl('',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    ext: new FormControl('',[Validators.pattern(/^\d{8,12}$/)]),
    sdt: new FormControl('', [Validators.pattern(/^\d{8,12}$/)]),
    email: new FormControl('', [Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
  });
  
  ///////
  genders = [{ id: 1, name: 'Male' }, { id: 2, name: 'Female' }, { id: 3, name: 'Other' }];
  thongTinChiTiet = new FormGroup({
    gioitinh: new FormControl(this.genders[0].name),
    ngaysinh: new FormControl(''),
    noisinh: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    nguyenquan: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    tongiao: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    dantoc: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    cmnd: new FormControl('123456789', [Validators.pattern(/^\d{8,12}$/)]),
    ngaycapcmnd: new FormControl(''),
    noicapcmnd: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    sohochieu: new FormControl('123456789', [Validators.pattern(/^\d{8,12}$/)]),
    ngaycaphochieu: new FormControl(''),
    ngayhethanhochieu: new FormControl(''),
    noicaphochieu: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    masothue: new FormControl('123456789',[Validators.pattern(/^\d{8,12}$/)]),
    quoctich: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    diachithuongtru: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
    diachihokhau: new FormControl('asd',[Validators.pattern(/^[A-Za-z0-9_\.]/)]),
  });

  //////Thông tin chung
  thongTinChung= new FormGroup({
    manhanvien: new FormControl(this.user.maNV),
    machamcong:new FormControl(''),
    calamviec: new FormControl(''),
    trangthailamviec: new FormControl(''),
  })
  show_edit_ttc(){
    this.show.edit_ttc=!this.show.edit_ttc;
    this.show.save_ttc=true;
  }
  show_save_ttc(){
    this.show.save_ttc=!this.show.save_ttc;
    this.show.edit_ttc=true;
  }



  //Văn phòng làm việc
  vanphonglamviec= new FormGroup({
    diachi1:new FormControl('asd'),
    diachi2:new FormControl('asd'),
    diachi3:new FormControl('asdasd'),
  });

  show_edit_vanphong(){
    this.show.edit_vanphong=!this.show.edit_vanphong;
    this.show.save_vanphong=true;
  }
  show_save_vanphong(){
    this.show.save_vanphong=!this.show.save_vanphong;
    this.show.edit_vanphong=true;
  }



  //////





  show = {
    edit: true,
    save: false,
    edit1: true,
    save1: false,
    inputvanphong:false,
    edit_ttc:true,
    save_ttc:false,
    edit_vanphong:true,
    save_vanphong:false
  }
  show1():void {
    this.show.edit = !this.show.edit;
    this.show.save = true;
    this.thongTinCaNhan.patchValue({
      ho:this.user.firstname,
      tenlot:this.user.middlename,
      ten:this.user.lastname,
    })

  }
  show2() {
    this.show.save = !this.show.save;
    this.show.edit = true;
    this.updateEmployee();
  

  }

  show3() {
    this.show.edit1 = !this.show.edit1;
    this.show.save1 = true;

  }
  show4() {
    this.show.save1 = !this.show.save1;
    this.show.edit1 = true;

  }




}
