import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { Employee } from 'src/app/Models/Employee';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  constructor(private employeeService:EmployeeService) { }
  public dataSource = new MatTableDataSource<Employee>();
  // public dataSource = new MatTableDataSource<User>(uer);


  user:Employee={id:'',
  anhcanhan:'',
  firstname:'',
  middlename:'',
  lastname:'',maNV:''
  ,chucvu:'',gioitinh:'',
  email:'',
  sdt:'',
  ext:'',
  ngaysinh:'',
  noisinh:'',
  nguyenquan:'',
  tongiao:'',
  dantoc:'',
  cmnd:'',
  ngaycapcmnd:'',
  sohochieu:'',
  ngayhethanhochieu:'',
  ngaycaphochieu:'',
  noicaphochieu:'',
  masothue:'',
  quoctich:'',
  diachithuongtru:'',
  diachihokhau:'',
  noicapcmnd:'',
  machamcong:'',
  trangthailamviec:'',
  calamviec:'', 
  }

 
  ngOnInit(): void {
    
  }
  

  createNhanvien(){
    this.employeeService.createNhanvien(this.user).subscribe(
      (res:any)=>{
        if(res.status == "success"){
          console.log(this.dataSource.data)   
          alert('Thêm thành công')
          window.location.reload();   
        }else{
          alert('thất bại')
        }
      }
    )
  }

 

}

