import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Task1Component } from './task1/task1.component';
import { DetailComponent } from './Component/detail/detail.component';
import { ListComponent } from './Component/list/list.component';
const routes: Routes = [{path:'task1',component:Task1Component},{path:'detail/:id',component:DetailComponent},{path:'list',component:ListComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
