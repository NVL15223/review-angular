const plugin = require('tailwindcss/plugin');
function genPxSizeMap(min, max) {
  const sizes = {};
  for (let size = min; size <= max; size++) {
    const sizeToAdd = `${size}px`;
    sizes[sizeToAdd] = sizeToAdd;
  }
  return sizes;
}

/** adds additional color utility classes to tailwind e.g. if called with ('caret', 'caret-color') will gen { .caret-pink: { caret-color: pink } ... }  */
function genColorUtilClasses(prefix, attribute) {
  return function ({ e, addUtilities, theme }) {
    const colors = theme('colors');
    const classes = {};

    for (key in colors) {
      classes[`.${e(`${prefix}-${key}`)}`] = {
        [attribute]: colors[key],
      };
    }

    addUtilities(classes);
  };
}

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#ffffff',
      black: '#000000',
      'black-1': '#050505',
      'black-2': '#222222',
      'black-5': '#7f7f7f',
      'gray-1': '#f2f2f2',
      'gray-2': '#8e8f98',
      'gray-3': '#dcdcdc',
      'gray-4': '#7f7f7f',
      'deep-red': '#af004b',
      'soft-red': '#fd6181',
      'warm-red': '#ff634e',
      raspberry: '#af004b',
      red: '#ff0000',
      pink: '#ff3bd5',
    },
    fontFamily: {
      maxis: ['Maxis', 'sans-serif'],
    },
    screens: {
      // default: 0 <-> 479 // portrait mobile // fluid width
      sm: '480px', // landscape mobile // fixed width // same design as portrait mobile
      md: '768px', // tablet // fixed width
      lg: '1014px', // laptop // fixed width
      xl: '1336px', // desktop // fixed width
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '15px',
        sm: '30px',
        md: '30px',
        lg: '30px',
        xl: '30px',
      },
    },
    extend: {
      fontSize: genPxSizeMap(1, 150),
      spacing: genPxSizeMap(1, 300),
      borderRadius: genPxSizeMap(1, 150),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [plugin(genColorUtilClasses('caret', 'caret-color')), require('@tailwindcss/forms')],
}
